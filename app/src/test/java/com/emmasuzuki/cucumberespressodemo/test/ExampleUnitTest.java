package com.emmasuzuki.cucumberespressodemo.test;

import android.os.Bundle;
import android.text.Editable;
import android.widget.EditText;

import org.junit.Before;
import org.junit.Test;
import android.test.AndroidTestCase;


import static org.junit.Assert.*;
import com.emmasuzuki.cucumberespressodemo.LoginActivity;

import org.mockito.Mockito;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;
import java.util.regex.Pattern;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 *
 */
public class ExampleUnitTest  {

    private LoginActivity tested;
    private Pattern emailPattern;

    @Before
    public void setUp() throws Exception {
        tested = spy(LoginActivity.class);
        emailPattern = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);
        tested.setEmailAddressMatcher(emailPattern);
    }

    @Test
    public void login_validateEmail_Valid() {
        String result = tested.validateEmail("email@email.com");
        assertEquals(null, result);
    }

    @Test
    public void login_validateEmail_Invalid() {
        String result = tested.validateEmail("email");
        assertEquals("Please enter your email", result);
    }
}