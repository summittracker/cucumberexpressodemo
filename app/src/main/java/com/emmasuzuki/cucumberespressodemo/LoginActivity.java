/*
 * Copyright (C) 2015 emmasuzuki <emma11suzuki@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH
 * THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package com.emmasuzuki.cucumberespressodemo;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import java.util.regex.Pattern;
import com.crashlytics.android.Crashlytics;
import io.fabric.sdk.android.Fabric;

/**
 * Demo Login activity class
 */
public class LoginActivity extends Activity {

    private static final String DEMO_EMAIL = "espresso@spoon.com";
    private static final String DEMO_PASSWORD = "lemoncake";

    private EditText emailEditText, passwordEditText;
    private View errorView;
    private Pattern emailAddressMatcher = android.util.Patterns.EMAIL_ADDRESS;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());

        setContentView(R.layout.login);

        emailEditText = findViewById(R.id.email);
        passwordEditText = findViewById(R.id.password);

        View submitButton = findViewById(R.id.submit);
        submitButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // validate fields
                emailEditText.setError(validateEmail(emailEditText.getText().toString()));
                passwordEditText.setError(validatePassword(passwordEditText.getText().toString()));

                if (emailEditText.getError() == null && passwordEditText.getError() == null) {
                    validateAccount();
                }
            }
        });
    }

    public String validateEmail(String email) {
        System.out.println( getEmailAddressMatcher().matcher(email).matches() );
        if (!getEmailAddressMatcher().matcher(email).matches()) {
            return "Please enter your email";
        } else {
            return null;
        }
    }

    public String validatePassword(String password) {
        if (password.isEmpty()) {
            return getString(R.string.msg_password_error);
        } else {
            return null;
        }
    }

    private void validateAccount() {
        if (errorView == null) {
            errorView = findViewById(R.id.error);
        }

        if (!emailEditText.getText().toString().equals(DEMO_EMAIL) || !passwordEditText.getText().toString().equals(DEMO_PASSWORD)) {
            errorView.setVisibility(View.VISIBLE);
        } else {
            errorView.setVisibility(View.GONE);
        }
    }

    public Pattern getEmailAddressMatcher() {
        return emailAddressMatcher;
    }

    public void setEmailAddressMatcher(Pattern emailAddressMatcher) {
        this.emailAddressMatcher = emailAddressMatcher;
    }

}
